struct Ferry {
    pos: (i32, i32),
    heading: i32,
}
impl Ferry {
    fn execute_action(&mut self, action: &Action) {
        match action {
            Action::North(n) => self.pos.1 +=  n,
            Action::East(n) => self.pos.0 += n,
            Action::South(n) => self.pos.1 -=  n,
            Action::West(n) => self.pos.0 -= n,
            // % operator actually remainder, not modulo
            Action::Left(n) => {
                self.heading = (self.heading - n + 360) % 360;
            },
            Action::Right(n) => {
                self.heading = (self.heading + n + 360) % 360;
            },
            Action::Forward(n) => {
                dbg!(&self.heading);
                match self.heading {
                    0 => self.pos.0 += n,
                    180 => self.pos.0 -= n,
                    90 => self.pos.1 -= n,
                    270 => self.pos.1 += n,
                    _ => panic!("Invalid heading"),
                }
            }

        }
    }
}
enum Action {
    North(i32),
    East(i32),
    South(i32),
    West(i32),
    Left(i32),
    Right(i32),
    Forward(i32),
}

fn main() {
    let input = std::fs::read_to_string("input/day12.txt").unwrap();
    let mut entries: Vec<Action> = input
        .lines()
        .map(|x|{
            match (x.chars().next().unwrap()) {
                'N' => Action::North(x[1..x.len()].parse::<i32>().unwrap()),
                'E' => Action::East(x[1..x.len()].parse::<i32>().unwrap()),
                'S' => Action::South(x[1..x.len()].parse::<i32>().unwrap()),
                'W' => Action::West(x[1..x.len()].parse::<i32>().unwrap()),
                'L' => Action::Left(x[1..x.len()].parse::<i32>().unwrap()),
                'R' => Action::Right(x[1..x.len()].parse::<i32>().unwrap()),
                'F' => Action::Forward(x[1..x.len()].parse::<i32>().unwrap()),
                _ => panic!("Invalid action"),
            }

        })
        .collect();

    let mut ship = Ferry {
        pos: (0, 0),
        heading: 0,
    };

    for action in entries {
        ship.execute_action(&action);
    }

    dbg!(ship.pos.1.abs() + ship.pos.0.abs());

}
