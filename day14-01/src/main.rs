use std::collections::HashMap;

#[derive(Copy, Clone, Debug)]
enum Instruction {
    Mask(u64, u64, u64),
    Mem(u64, u64)
}
fn parse_instruction(string: &str) -> Instruction {
    let len = string.len();
    if (&string[0..4] == "mask") {
        let mask_str = &string[len-36..len];
        let mut pos_x: u64 = 0;
        let mut pos_1: u64 = 0;
        let mut pos_0: u64 = 0;

        for (i, c) in mask_str.chars().enumerate() {
            let exponent = (35-i) as u32;
            match c {
                'X' => pos_x |= 2_u64.pow(exponent),
                '1' => pos_1 |= 2_u64.pow(exponent),
                '0' => pos_0 |= 2_u64.pow(exponent),
                _ => panic!("Invalid mask char"),
            };
        }

        return Instruction::Mask(pos_x, pos_0, pos_1)
    }
    if (&string[0..3] == "mem") {
        let tmp: Vec<&str> = string.split(" = ").collect();
        let val: u64 = tmp[1].parse::<u64>().expect("Invalid value");
        let addr: u64 = tmp[0][4..tmp[0].len() - 1].parse::<u64>().expect("Invalid address");
        return Instruction::Mem(addr, val)

    }

    dbg!(&string[0..5]);
    panic!("Invalid instruction");
}

fn main() {
    let input = std::fs::read_to_string("input/day14.txt").unwrap();
    let mut instructions: Vec<Instruction> = input
        .lines()
        .map(|x| parse_instruction(x))
        .collect();

    let mut mask: (u64, u64, u64) = (0, 0, 0);
    let mut memory: HashMap<u64, u64> = HashMap::new();

    for ins in instructions.iter() {
        match ins {
            Instruction::Mask(pos_x, pos_0, pos_1) => mask = (*pos_x, *pos_0, *pos_1),
            Instruction::Mem(addr, val) => {
                let cur_val = memory.get(&addr).unwrap_or(&0);
                let res = (val & mask.0 & !mask.1) | mask.2;

                 memory.insert(*addr, res);
            }
        }
    }

    let sum = memory.iter().fold(0, |acc, (_, &val)| acc+val);
    dbg!(sum);
}
