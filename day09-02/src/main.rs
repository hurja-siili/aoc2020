
fn main() {
    let input = std::fs::read_to_string("input/day09.txt").unwrap();
    let mut entries: Vec<&str> = input
        .lines()
        .collect();
    let mut preamble: [u32; 25] = [0; 25];

    for i in 0..25 {
        preamble[i] = entries[i].parse::<u32>().unwrap();
    }

    for i in 25..entries.len() {
        let num = entries[i].parse::<u32>().unwrap();
        if pair_sum_exists(num, &preamble) {
            preamble[i % 25] = num;
            continue
        } else {
            dbg!(num);
            break
        }
       
    }

}

fn pair_sum_exists(target: u32, list: &[u32]) -> bool {
    for (i_idx, i) in list.iter().enumerate() {
        for (j_idx, j) in list.iter().enumerate() {
            if (j_idx == i_idx) {
                continue;
            }
            if i + j == target {
                return true
            }

        }
    }

    return false
}
