fn main() {
    let input = std::fs::read_to_string("input/day13.txt").unwrap();
    let mut entries: Vec<&str> = input
        .lines()
        .collect();

    let cur_time = entries[0].parse::<u32>().unwrap();
    let bus_numbers: Vec<u32> = entries[1].split(",").filter(|x| *x != "x").map(|x| x.parse::<u32>().unwrap()).collect();

    let time_to_wait = bus_numbers.iter().map(|num| {
        let departures_since_start = cur_time / num;
        (departures_since_start + 1) * num - cur_time

    }).collect::<Vec<u32>>();

    let res = time_to_wait.iter().zip(bus_numbers).min_by(|a, b| a.0.cmp(b.0)).map(|(wait, num)| wait * num);
    dbg!(res);
}
