use std::collections::{HashSet, VecDeque};
use itertools::Itertools;

fn main() {
    let input = std::fs::read_to_string("input/day22.txt").unwrap();
    let mut lines: Vec<&str> = input
        //.split(|x| x =='\n')
        .lines()
        .collect();

    let (deck_1_str, deck_2_str) = lines.split(|&x| x == "").collect_tuple().unwrap();

    let mut deck_1: VecDeque<_>  = deck_1_str[1..].iter().map(|x| x.parse::<i32>().unwrap()).collect();
    let mut deck_2: VecDeque<_>  = deck_2_str[1..].iter().map(|x| x.parse::<i32>().unwrap()).collect();

    let deck_1_won = play_combat_game(&mut deck_1, &mut deck_2);
    if deck_1_won {
        dbg!(&deck_1);
        dbg!(&deck_2);
        let points = deck_1.iter().rev().enumerate().fold(0, |acc, (idx, val)| {
            acc + ((idx + 1) as i32 * val)
        });
        dbg!(points);
    } else {
        dbg!(&deck_1);
        dbg!(&deck_2);
        let points = deck_2.iter().rev().enumerate().fold(0, |acc, (idx, val)| {
            acc + ((idx + 1) as i32 * val)
        });
        dbg!(points);
    }

}

fn play_combat_game(deck_1: &mut VecDeque<i32>, deck_2: &mut VecDeque<i32>) -> bool {

    let mut list_of_rounds: HashSet<(String, String)> = HashSet::new();
    loop {
        let deck_1_str = deck_1.iter().fold("".to_string(), |acc, num| {
           format!("{}{}", acc, num)
        });
        let deck_2_str = deck_2.iter().fold("".to_string(), |acc, num| {
           format!("{}{}", acc, num)
        });
        if !list_of_rounds.insert((deck_1_str, deck_2_str)) {
            return true
        }

        let card_1 = deck_1.pop_front();
        let card_2 = deck_2.pop_front();
        match (card_1, card_2) {
            (Some(card_1), Some(card_2)) => {
                if card_1 <= deck_1.len() as i32 && card_2 <= deck_2.len() as i32 {
                    let d1: Vec<i32> = deck_1.iter().take(card_1 as usize).map(|x| *x).collect();
                    let d2: Vec<i32> = deck_2.iter().take(card_2 as usize).map(|x| *x).collect();
                    let card_1_won = play_combat_game(&mut d1.into(),
                                                      &mut d2.into()
                    );
                    if card_1_won {
                        deck_1.push_back(card_1);
                        deck_1.push_back(card_2);
                    } else {
                        deck_2.push_back(card_2);
                        deck_2.push_back(card_1);
                    }
                } else {
                    let card_1_won = card_1 > card_2;
                    if card_1_won {
                        deck_1.push_back(card_1);
                        deck_1.push_back(card_2);
                    } else {
                        deck_2.push_back(card_2);
                        deck_2.push_back(card_1);
                    }

                }

            },
            (None, Some(card_2)) => {
                deck_2.push_front(card_2);
                return false
            },
            (Some(card_1), None) => {
                deck_1.push_front(card_1);
                return true
            },
            (None, None) => {
                panic!("Impossible state");
            }

        }

    }
}
