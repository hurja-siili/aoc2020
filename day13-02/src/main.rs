fn main() {
    let input = std::fs::read_to_string("input/day13.txt").unwrap();
    let mut entries: Vec<&str> = input
        .lines()
        .collect();

    let bus_schedule: Vec<(usize, usize)> = entries[1].split(",").enumerate().filter(|(_, x)| *x != "x").map(|(i, x)| (i ,x.parse::<usize>().unwrap())).collect();
    let bus_schedule: Vec<(i64, i64)> = bus_schedule.iter().map(|(a, b)| (*a as i64, *b as i64)).collect();
    let product = bus_schedule.iter().fold(1, |acc, (_, interval)| acc*interval);
    dbg!(product);


    let res = bus_schedule.iter().map(|(depart, num)| -depart*(product/num)*mod_inv(product/num, *num)).sum::<i64>().rem_euclid(product);
    dbg!(res);
}


fn mod_inv(a: i64, b: i64) -> i64 {
    let (x, y) = egcd(a, b);
    if (a * x + b*y) == 1 {
        return x
    } else {
        panic!("{}, {}", a, b);
    }
}

fn egcd(a: i64, b: i64) -> (i64, i64) {
    match (a, b) {
        (_, 0) => (1, 0),
        (a, b) => {
            let (q, r) = div_rem(a, b);
            let (s, t) = egcd(b, r);
            (t, s - q * t)
        }

    }
}

pub fn div_rem<T: std::ops::Div<Output=T> + std::ops::Rem<Output=T> + Copy>(x: T, y: T) -> (T, T) {
    let quot = x / y;
    let rem = x % y;
    (quot, rem)
}
