use std::collections::HashMap;
use std::iter::Iterator;

#[derive(Clone, Copy, Debug, PartialEq)]
enum Square {
    Floor,
    Empty,
    Occupied,
}

fn main() {
    let input = std::fs::read_to_string("input/day11.txt").unwrap();
    let mut entries: Vec<&str> = input
        .lines()
        .collect();

    let mut seat_layout: Vec<Vec<Square>> = Vec::new();
    let rows = entries.len();
    let columns = entries[0].len();

    for line in entries.iter() {

        let mut row = Vec::new();
        for symbol in line.chars() {
            match symbol {
                '.' => row.push(Square::Floor),
                '#' => row.push(Square::Occupied),
                'L' => row.push(Square::Empty),
                _ => panic!("Unrecognised symbol"),
            }
        }
        seat_layout.push(row);
    }

    loop {
        let prev_state = seat_layout.clone();
        let mut changed = false;

        for i in (0..rows) {
            for j in (0..columns) {
                let adjacent_seats = find_visible_seats(i, j, &prev_state);
                let num_occupied: u8 = adjacent_seats.iter().fold(0 as u8, |acc ,x| match x {
                    Some(Square::Occupied) => acc + 1,
                    _ => acc,
                });

                match prev_state.get(i).and_then(|x| x.get(j)) {
                    Some(Square::Empty) => {
                        if num_occupied == 0 {
                            seat_layout[i][j] = Square::Occupied;
                            changed = changed || true;
                        }
                    },
                    Some(Square::Occupied) => {
                        if num_occupied >= 5 {
                            seat_layout[i][j] = Square::Empty;
                            changed = changed || true;
                        }
                    },
                    _ => {},
                }
            }
        }

        if !changed {
            break
        }
    }


    let mut num_occupied = 0;
    for i in (0..rows) {
        for j in (0..columns) {
            match seat_layout.get(i).and_then(|x| x.get(j)) {
                Some(Square::Occupied) => num_occupied += 1,
                _ => {},
            }
        }
    }

   dbg!(num_occupied);

}

fn find_visible_seats<'a>(row: usize, column: usize, seat_layout: &'a Vec<Vec<Square>>) -> [Option<&'a Square>; 8] {
    let mut result = [Some(&Square::Floor); 8];


    let mut i = 1;
    // Dirty underflow when row or column == 0, should be ok
    loop {
        if result[0] == Some(&Square::Floor) {
            result[0] = seat_layout.get(row-i).and_then(|x| x.get(column-i).clone());
        }
        if result[1] == Some(&Square::Floor) {
            result[1] = seat_layout.get(row-i).and_then(|x| x.get(column).clone());
        }
        if result[2] == Some(&Square::Floor) {
            result[2] = seat_layout.get(row-i).and_then(|x| x.get(column+i).clone());
        }

        if result[3] == Some(&Square::Floor) {
            result[3] = seat_layout.get(row).and_then(|x| x.get(column-i).clone());
        }
        if result[4] == Some(&Square::Floor) {
            result[4] = seat_layout.get(row).and_then(|x| x.get(column+i).clone());
        }

        if result[5] == Some(&Square::Floor) {
            result[5] = seat_layout.get(row+i).and_then(|x| x.get(column-i).clone());
        }
        if result[6] == Some(&Square::Floor) {
            result[6] = seat_layout.get(row+i).and_then(|x| x.get(column).clone());
        }
        if result[7] == Some(&Square::Floor) {
            result[7] = seat_layout.get(row+i).and_then(|x| x.get(column+i).clone());
        }
        i += 1;
        if result.iter().all(|&x| x != Some(&Square::Floor)) {
            break
        }
    }

    result
}
