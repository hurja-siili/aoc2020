use itertools::Itertools;
use std::collections::HashMap;

fn main() {
    let input = std::fs::read_to_string("input/day04.txt").unwrap();
    let entries: Vec<&str> = input
        .lines()
        .collect();
    let mut valid_passports = 0;
    let mut rejected_passports = 0;

    let mandatory_fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];
    let mut passports: Vec<String> = Vec::new();
    let mut cur_passport = String::new();
    for entry in entries.iter() {
        if *entry == "" {
            passports.push(cur_passport);
            cur_passport = String::new();
        } else {
            cur_passport.push(' ');
            cur_passport.push_str(entry);
        }
    }
    passports.push(cur_passport);

    'passport: for passport in passports.iter() {

        let fields = passport.split_whitespace()
                .flat_map(|p| p.split(':'))
                .tuples()
                .collect::<HashMap<_,_>>();

        for field in mandatory_fields.iter() {
            if !fields.contains_key(field){
                rejected_passports += 1;
                continue 'passport;
            }
        }

        valid_passports += 1;
    }

    dbg!(valid_passports);
    dbg!(rejected_passports);
    dbg!(passports);
}
