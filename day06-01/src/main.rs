use std::collections::HashSet;

fn main() {
    let input = std::fs::read_to_string("input/day06.txt").unwrap();
    let mut entries: Vec<&str> = input
        .lines()
        .collect();

    let mut answers: Vec<HashSet<char>> = Vec::new();
    let mut sum = 0;

    for entry in entries.iter() {
        if *entry == "" {
            let mut  tmp: HashSet<char> = answers[0].clone();
            let intersection = answers.iter().fold(tmp, |a, b| a.intersection(b).map(|x| *x).collect());
            dbg!(intersection.len());
            sum += intersection.len();
            answers = Vec::new();
        } else {
            let mut answer: HashSet<char> = HashSet::new();
            for question in entry.chars() {
                answer.insert(question);
            }
            answers.push(answer);
        }
    }
    let mut  tmp: HashSet<char> = answers[0].clone();
            let intersection = answers.iter().fold(tmp, |a, b| a.intersection(b).map(|x| *x).collect());
            dbg!(intersection.len());
            sum += intersection.len();
            answers = Vec::new();

    dbg!(sum);

}
