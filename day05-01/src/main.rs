use std::collections::HashMap;

fn main() {
    let input = std::fs::read_to_string("input/day05.txt").unwrap();
    let mut entries: Vec<&str> = input
        .lines()
        .collect();

    let entries: Vec<(&str, &str)> = entries.iter().map(|x| x.split_at(7)).collect();
    let mut seats: HashMap<u32, bool> = HashMap::new();

    let num_rows = 128;
    let num_columns = 8;

    for i in (0..num_rows) {
        for j in (0..num_columns) {
           seats.insert((i*8 + j ) as u32, false);
        }
    }

    let mut max_seat = 0;
    let row_nums: Vec<i32> = (0..7).rev().map(|x: u32| 2_i32.pow(x)).collect();
    let col_nums: Vec<i32> = (0..3).rev().map(|x: u32| 2_i32.pow(x)).collect();

    for (rows, cols) in entries.iter(){

        let mut row = 0;
        let mut col = 0;

        for (idx, i)  in rows.chars().enumerate() {
            if(i == 'B') {
                row += row_nums[idx];
            }
        }
        for (idx, i)  in cols.chars().enumerate() {
            if(i == 'R') {
                col += col_nums[idx];
            }
        }

        let seat_num = row * 8 + col;
        seats.insert(seat_num as u32, true);
        if seat_num > max_seat {
            max_seat = seat_num;
        }

    }

    for seat_idx in (8..127*8+7){

        let seat = seats.get(&seat_idx).unwrap();
        if !seat &&
            (seats.get(&(seat_idx-1)).unwrap() == &true) &&
            (seats.get(&(seat_idx+1)).unwrap() == &true)
        {
            dbg!(seat_idx);
        }
    }
}
